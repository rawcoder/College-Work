Handwritten digits recognition
==============================

Handwritten digits recognition system implemented in Octave.
Uses MNIST database for training.

How-to
------

* Create gimp.h files of size 28*28 each for the image of the handwritten digits.
* Use gimpc_to_mat.sh to convert the .h files to .mat.
* Finally use `$ octave recognizer.m` to get the output.
