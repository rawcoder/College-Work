#!/bin/bash
# generate_graph.sh
# Generate 'Accuracy vs hidden units' graphs using data from files 'stats' & 'results'.

# set "iters" to default if not supplied
iters=${1:-150}
touch acc hu racc rhu
sort -t'|' -n -k 3 stats | awk -F'|' '$1 == '$iters' { printf("%s\n",$3)>>"hu";  printf("%s\n", $5)>>"acc"  }'
sort -t'|' -n -k 3 results | awk -F'|' '$1 == '$iters' { printf("%s\n",$3)>>"rhu"; printf("%s\n", $5)>>"racc" }'

octave -q --persist --eval "acc = load('acc'); hu = load('hu');
	racc = load('racc'); rhu = load('rhu');
	plot(hu, acc, '--xr', 'markerfacecolor', 'r', 'markersize', 10, 'linewidth', 2);
	hold on;
	plot(rhu, racc, '--ob', 'markerfacecolor', 'b', 'markersize', 10, 'linewidth', 2);
	hl = legend('Training set', 'Test set');
	set(hl, 'linewidth', 2, 'fontsize', 15);
	xlabel('# of hidden units', 'fontsize', 15);
	ylabel('Accuracy in %', 'fontsize', 15);
	title('Accuracy vs # of hidden units (iters=$iters)', 'fontsize', 15);
	hcx = get(gcf, 'currentaxes');
	set(hcx, 'linewidth', 2, 'fontsize', 15);
	printf('Graph Generated.\nPress ^D to exit\n');"

rm acc hu racc rhu
